import React, {Component} from 'react';
import TopBar from './components/TopBar.js';
import ListColumn from './components/ListColumn';
import ProductDetails from './components/ProductDetails';

class App extends Component {
  
  render() {
          return(
            <div className="App">
              <TopBar/>
                <ListColumn/>
                <ProductDetails/>
              
            </div>
      );
    }
} 

export default App;
