import React, {Component} from 'react';
import ProductList from './ProductList';
import ProductDetails from './ProductDetails';


class ListColumn extends Component {

    constructor(props){
        super(props);

        this.state={
            
            error:null,
            isLoaded: false,
            products:[],
            id: null
        }

    }

    getId = (id) => {
        
        console.log(id);
        this.state.products.map(product =>{
            console.log(id);
            if(id === product.id){
                this.setState({
                    id:id
                })
            }
        })
        console.log(id);
    }

    componentDidMount() {   //load hosted json data into state
        const proxyurl = "https://cors-anywhere.herokuapp.com/";
        const url = "https://public-rand.s3-ap-southeast-2.amazonaws.com/data.json";
        fetch(proxyurl + url)
          .then(res => res.json())
          .then(
             
            result => {
                console.log(result)
              this.setState({
                isLoaded: true,
                products: result
                
              });
              
            },
            
            (error) => {
              this.setState({
                isLoaded: true,
                error
              });
            }
          )
          .catch(() => console.log("Can’t access " + url + " response. Blocked by browser?"))
      }
      
    render(){
        
        const {error, isLoaded, products, id} = this.state; //deconstruct and gets the products object from props
        
        if(error){
            return <div> Error: {error.message}</div>;
        }else if(!isLoaded){
            return <div>Loading...</div>;
        }else{
                console.log(this.state);  
                console.log(this.props);    
                return(
                    <div className="container">
                        <div className="row">
                            <div className="col s12 m5 ">
                                <ProductList products={products} callbackId = {this.getId} />
                            </div>
                            <div className="col s12 m6 offset-m1" id="prodDetails">
                                <ProductDetails products={products} id={id} />
                            </div>
                        </div>
                    </div>
                )
            }
            
    }
    
}


export default (ListColumn);