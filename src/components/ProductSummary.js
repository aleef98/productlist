import React, {Component} from 'react';

class ProductSummary extends Component {

    constructor(props) {
        super(props);
        console.log(props);
        
    }

    sendId2 = (id) =>{
        this.props.callbackId2(id);
    }
    

 
    render(){
        
                    return(
                    
                    <div className="card">
                        <div className="card-content">
                            <span className="card-title">{this.props.product.title}</span>
                            <p>{this.props.product.summary}</p>
                        </div>
                    </div>
                    
                    )
            
        }


}
export default ProductSummary;