import React from 'react';



const ProductDetails = ({products, id}) => {

    console.log(id)
        return(
            <div className="container section">
                
                    {products && products.map(product => {
                        if(id === null){
                            console.log(product.id)
                            return(
                                <div>
                                    <p></p>
                                </div>
                            )
                            
                        }
                        else if(id === product.id){
                            
                            return(
                                <div className="card">
                                    <div className="card-content">
                                        <span className="card-title">{product.title}</span>
                                        <p>{product.summary}</p>
                                    </div>
                                </div>
                            )
                        }
                    
                    })}    
            </div>
        )
    
}

export default (ProductDetails);