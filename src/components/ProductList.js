import React, {Component} from 'react';
import ProductSummary from './ProductSummary';



class ProductList extends Component {
    constructor(props) {
        super(props);
        console.log(props);
        this.state = { 
            clickedBtn: null
         }
    }

   
    sendId = (id) =>{
        this.props.callbackId(id);
    }

    handleClick(product) {
        this.sendId(product.id);
        console.log(this.props);
        console.log(this.state);
        
        this.buttonClicked(product.id);
        
    }

    buttonClicked = clickedBtn =>{
        this.setState({clickedBtn}, () => console.log(this.state))
    }

    render(){
        return(
            
            <div className="section">
                {this.props.products && this.props.products.map(product => {
                    return(
                        <button className = {product.id  === this.state.clickedBtn ? 'clicked' : ''} onClick={() => this.handleClick(product)}>
                        <div className="card">
                            <ProductSummary product={product} key={product.id}  />  
                        </div>
                        </button>
                        
                    )
                })}
            </div>
        )
    }
}

export default ProductList;